import React, { Component } from 'react';
import './App.scss';
import Header from './components/Header.jsx'
import Action from './components/Action.jsx'
import Options from './components/Options.jsx'
// import Option from './components/Options.jsx'


class IndecisionApp extends React.Component {

  state = {
    options: []
  }

  handleDeleteOption = optionToRemove =>{
    // console.log(option);
    this.setState((prevState) => ({
      options: prevState.options.filter((newOptions) => {
        return optionToRemove !== newOptions;
      })
    })) ;
  }

  handleReset = () => {
    let options = [];
    this.setState({options});
    console.log("reset hoche");
  }

  handleAddOption= option => {
    let options = this.state.options;
    options = options.concat(option);
    this.setState({options});
    console.log("add hoche");
  }

 render(){
    const title="Todo App";
    const subtitle="Build your ToDO"
   return(
    <div className="todoDiv relative">
      <div className="contentBgDiv">

      </div>
      <div className="contentDiv">
        <Header title={title} subtitle={subtitle} />
        <Action handleAddOption={this.handleAddOption} handleReset={this.handleReset}/>
        <Options options={this.state.options} handleDeleteOption={this.handleDeleteOption}/>
      </div>
    </div>
    
   );
 }
}

export default IndecisionApp;
