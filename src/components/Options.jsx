import React, { Component } from 'react';
import Option from './Option';


class Options extends React.Component{
    
    render(){
        return(
            <div className="optionsDiv">
                <h5 className="text-center optionsHeader"><u>Your Schedule</u></h5>
                
                {this.props.options.map((option) => <Option key={option} optionText={option}  handleDeleteOption={this.props.handleDeleteOption}/>)}
            </div>
        );
    }
}
export default Options;