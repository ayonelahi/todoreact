import React from 'react';

export default class Action extends React.Component {

  handleAddOption = e => {
    e.preventDefault();
    const option = e.target.elements.option.value.trim();
    this.props.handleAddOption(option);
  }
  
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col col-md-6 col-md-offset-3 actionDiv noOutline text-center">
            <form onSubmit={this.handleAddOption}>
              <button className="resetButton noOutline borRad5 bgTrans" type='button' onClick={this.props.handleReset}>Reset</button>
              <div className="addingDiv">
                <input className="inputField noOutline borRad5 bgTrans" type="text" name="option" placeholder="Enter Your Schedule here!!"/>
                <button className="addButton noOutline borRad5 bgTrans" type='submit'>Add Option</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      
    );
  }
}
