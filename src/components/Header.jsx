import React, { Component } from 'react';
import logo from '../logo.svg';

class Header extends React.Component{
    render(){
        return(
            <div className="headerDiv">
                <div className="w-100 text-center">
                    <img src={logo} className="App-logo" alt="logo" />
                </div>
                <h3 className="text-center header">{this.props.title}</h3>
                <p className="text-center subheader" >{this.props.subtitle}</p>
            </div>
            
        );
    }
}
export default Header;