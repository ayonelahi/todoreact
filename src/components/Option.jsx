import React, { Component } from 'react';


class Option extends Component {
 
    render() { 
        return ( 
            <div className="container">
                <div className="row">
                    <div className="col col-md-6 col-md-offset-6">
                        <div className="optionDiv">
                            <p className="singleOptionP">{this.props.optionText}</p>
                            <button
                                className="singleDeleteButton noOutline borRad5 bgTrans"
                                onClick={(e) => {
                                this.props.handleDeleteOption(this.props.optionText);
                                }}  
                            >
                            Done
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            
        );
    }
}
 
export default Option;