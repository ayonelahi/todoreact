import React, { Component } from 'react';
import './App.scss';
import logo from './logo.svg';

class IndecisionApp extends React.Component {
  constructor(props){
    super(props);
    this.handleDeleteoptions=this.handleDeleteoptions.bind(this);
    this.handlePick = this.handlePick.bind(this);
    this.handleAddOption = this.handleAddOption.bind(this);
    this.state={
     options: []
    };
  }
  handleDeleteoptions(){
    this.setState(
      ()=>{
        return{
          options:[]
      };
      });
  }
  handlePick() {
    const randomNum = Math.floor(Math.random() * this.state.options.length);
    const option = this.state.options[randomNum];
    alert(option);
  }
  handleAddOption(option) {
    if (!option) {
      return 'Enter valid value to add item';
    } else if (this.state.options.indexOf(option) > -1) {
      return 'This option already exists';
    }

    this.setState((prevState) => {
      return {
        options: prevState.options.concat(option)
      };
    });
  }

  render() {
    const title='Todo';
    const subtitle='make your todo list';

    return (
      <div>
        <Header title={title} subtitle={subtitle} />
        <Action
        hasOptions={this.state.options.length > 0}
        handlePick={this.handlePick}
        />
        <Options
          options={this.state.options}
          handleDeleteoptions={this.handleDeleteoptions} 
          
          />
        <AddOption
         handleAddOption={this.handleAddOption}
        />
      </div>
    );
  }
}

class Header extends React.Component {
  render() {
    return (
      <div className='App-header'>
      <img src={logo} className="App-logo" alt="logo" />
      <h1>{this.props.title}</h1> 
      <h3>{this.props.subtitle}</h3> 
      </div>
    );
  }
}

class Action extends React.Component {
  render() {
    return (
      <div>
        <button
         onClick={this.props.handlePick}
         disabled={!this.props.hasOptions}
        >
        What should I do?</button>
      </div>
    );
  }
}

class Options extends React.Component {
  render() {
    return (
      <div>
            <button onClick={this.props.handleDeleteoptions}>Delete All</button>
      {
        this.props.options.map((option) => <Option key={option} optionText={option} />)
      }
      </div>
    );
  }
}

class Option extends React.Component {
  render() {
    return (
      <div>
        {this.props.optionText}
      </div>
    );
  }
}

class AddOption extends React.Component {
  constructor(props) {
    super(props);
    this.handleAddOption = this.handleAddOption.bind(this);
    this.state = {
      error: undefined
    };
  }
  handleAddOption(e) {
    e.preventDefault();

    const option = e.target.elements.option.value.trim();
    const error = this.props.handleAddOption(option);

    this.setState(() => {
      return { error };
    });
  }
  render() {
    return (
      <div>
        {this.state.error && <p>{this.state.error}</p>}
        <form onSubmit={this.handleAddOption}>
          <input type="text" name="option" />
          <button>Add Option</button>
        </form>
      </div>
    );
  }
}

export default IndecisionApp;
